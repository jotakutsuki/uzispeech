package com.cobis.uzispeech;

import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edittext)
    EditText editText;
    @BindView(R.id.playButton)
    Button playButton;

    @BindView(R.id.holaButton)
    Button holaButton;
    @BindView(R.id.sordoButton)
    Button sordoButton;
    @BindView(R.id.restaurantButton)
    Button restaurantButton;
    @BindView(R.id.textview)
    TextView textView;
    @BindView(R.id.hearButton)
    Button hearButton;

    private TextToSpeech mTts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mTts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS){
                    int result = mTts.setLanguage(new Locale("es", "ES"));

                    if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED){
                        Toast.makeText(MainActivity.this, "Error, idioma no soportado", Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(MainActivity.this, "Praparado para hablar...", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        holaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTts.speak("Hola", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        sordoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTts.speak("Soy Sordo", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        restaurantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTts.speak("¿donde hay un restaurante?", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTts.speak(editText.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        hearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, new Locale("es", "ES"));
                startActivityForResult(intent, 10);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10
                && resultCode == RESULT_OK
                && data != null){
            ArrayList<String> stringArrayListExtra = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            textView.setText(stringArrayListExtra.get(0));
        }else{
            Toast.makeText(MainActivity.this, "No soporta microfono", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onDestroy() {
        if (mTts != null){
            mTts.stop();
            mTts.shutdown();
        }
        super.onDestroy();
    }
}
